# Improbible

:wave:

## Getting started

```bash
cd client
cp .env.sample .env

# edit .env ...

npm install
npm run build
```

:rocket:


## Start Storybook

```
cd client
npm run storybook
```