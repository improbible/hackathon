import { configure } from '@storybook/react';
import './../src/theme/app.less';

function loadStories() {
  require('../src/theme/global.story');
  require('../src/components/countdown/Countdown.story');
}

configure(loadStories, module);
