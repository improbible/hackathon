import 'babel-polyfill';

import 'theme/app.less';

import React from 'react';
import ReactDOM from 'react-dom';
import ReactGA from 'react-ga';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { useBasename } from 'history';

import createStore from 'createStore';
import routes from './routes';
import { firebaseConfig } from './config';

injectTapEventPlugin();

const gaEnabled = __GOOGLE_ANALYTICS__ !== null;

if (gaEnabled) {
    ReactGA.initialize(__GOOGLE_ANALYTICS__);
}

function handleRouteUpdate() {
    if (gaEnabled) {
        const hash = window.location.hash;
        const page = hash.substr(1, hash.length - 1) + window.location.search;
        ReactGA.set({ page });
        ReactGA.pageview(page);
    }
}

let _store;
const start = (options, routes) => {
    const initialState = {};
    if (!_store) {
        _store = createStore(initialState, firebaseConfig);
    }
    ReactDOM.render(
        <Provider store={_store}>
            <Router
                history={hashHistory}
                routes={routes}
                onUpdate={handleRouteUpdate}
            />
        </Provider>,
        document.getElementById(options.rootId)
    );
};

const options = {
    rootId: 'app',
};

if (module.hot) {
    module.hot.accept('./routes', () => {
        setTimeout(() => {
            console.log('hot reloading routes');
            ReactDOM.unmountComponentAtNode(document.getElementById('app'));
            start(options, routes);
        });
    });
}

start(options, routes);
