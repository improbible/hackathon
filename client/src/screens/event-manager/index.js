import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    firebaseConnect,
    isLoaded,
    isEmpty,
    dataToJS,
} from 'react-redux-firebase';

import stepService from 'services/event-service';
import { getStepFromEvent, getEventById, getStepById } from 'utils/selectors';
import { StepSelector } from 'components/step-selector';
import { ImproControls } from 'components/impro-controls';
import improService from 'services/impro-service';

@firebaseConnect([{ path: '/events' }, { path: '/steps' }])
@connect(({ props, firebase }) => ({
    eventById: dataToJS(firebase, `/events`),
    stepById: dataToJS(firebase, `/steps`),
}))
class EventManager extends Component {
    render() {
        const { params, eventById, stepById, firebase } = this.props;
        const { eventId } = params;
        if (!isLoaded(eventById)) {
            return null;
        }

        const event = eventById[eventId];
        const { activeStep } = event;
        const currentStep = activeStep === null ? null : stepById[activeStep];

        return (
            <div className="container">
                <h1 className="page-header">{event.name}</h1>
                {event && (
                    <StepSelector
                        onSelect={stepId => {
                            stepService
                                .connect(firebase)
                                .gotoStep({ eventId, stepId });
                        }}
                        steps={event.steps.map(stepId => stepById[stepId])}
                        selectedId={event.activeStep}
                    />
                )}
                <div>
                    {currentStep &&
                        currentStep.type === 'impro' && (
                            <ImproControls
                                step={currentStep}
                                startTimer={() => {
                                    improService
                                        .connect(firebase)
                                        .startTimer(currentStep.id);
                                }}
                                endTimer={() => {
                                    improService
                                        .connect(firebase)
                                        .endTimer(currentStep.id);
                                }}
                                startVote={() => {
                                    improService
                                        .connect(firebase)
                                        .startVote(currentStep.id);
                                }}
                                endVote={() => {
                                    improService
                                        .connect(firebase)
                                        .endVote(currentStep.id);
                                }}
                            />
                        )}
                </div>
            </div>
        );
    }
}

// connect to component to access props.dispatch
export default connect(state => ({}))(EventManager);
