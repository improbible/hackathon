import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    dataToJS,
    firebaseConnect,
    isEmpty,
    isLoaded,
} from 'react-redux-firebase';

import Countdown from './../../components/countdown';

@firebaseConnect(props => [
    {
        path: '/hackathon',
    },
])
@connect(({ firebase }) => ({
    infos: dataToJS(firebase, '/hackathon'),
}))
class Hackathon extends Component {
    componentWillMount() {}

    render() {
        const {infos} = this.props;
        if(!infos) {
            return null;
        }

        const {message, startAt, deadline} = infos;

        return <div className="hackathon container text-center">
            <div className="row">
                <div className="col-xs-12" style={{padding: '2rem'}}>
                    <Countdown
                        startAt={new Date(startAt)}
                        deadline={new Date(deadline)}
                    />
                </div>
                <div className="col-xs-12">
                    <p
                        style={{padding: '2rem 5rem'}}
                        className="message"
                        dangerouslySetInnerHTML={{__html: message}}
                    />
                </div>
            </div>
        </div>

    }
}

// connect to component to access props.dispatch
export default connect(state => ({}))(Hackathon);
