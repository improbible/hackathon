import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { Organization } from 'components/organization';

import {
    dataToJS,
    firebaseConnect,
    isEmpty,
    isLoaded,
} from 'react-redux-firebase';

@firebaseConnect(props => [
    {
        path: '/organizations',
    },
])
@connect(({ firebase }) => ({
    organizations: dataToJS(firebase, '/organizations'),
}))
class Home extends Component {
    componentWillMount() {}

    getOrganizations() {
        return Object.keys(this.props.organizations).map(id => {
            return this.props.organizations[id];
        });
    }

    render() {
        const { firebase, organizations } = this.props;

        return (
            <div>
                <div className="container">
                    <img
                        src="/public/assets/improbible-logo.png"
                        alt="improbible"
                        className="img img-responsive"
                    />
                    <a
                        href="/#/org/-KvKtBzawQq2BCqEluGE/event/-KvKtBzawQq2BCqEluGD"
                        className="btn btn-primary btn-lg btn-block"
                    >
                        <i className="fa fa-book" /> Improviser
                    </a>
                </div>
            </div>
        );
    }

    renderBackup() {
        const { firebase, organizations } = this.props;

        return (
            <div>
                <div className="container">
                    <img
                        src="/public/assets/improbible-logo.png"
                        alt="improbible"
                        className="img img-responsive"
                    />
                    {/* TODO: geolocalize nearby events and suggest organization based on location */}
                    {!isLoaded(organizations) ? (
                        'Loading'
                    ) : isEmpty(organizations) ? (
                        'Todo list is empty'
                    ) : (
                        <ul>
                            {this.getOrganizations().map(organization => {
                                return (
                                    <li key={organization.id}>
                                        <Link to={`/org/${organization.id}`}>
                                            <Organization {...organization} />
                                        </Link>
                                    </li>
                                );
                            })}
                        </ul>
                    )}
                </div>
            </div>
        );
    }
}

// connect to component to access props.dispatch
export default connect(state => ({}))(Home);
