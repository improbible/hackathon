import React from 'react';

import { storiesOf } from '@storybook/react';

storiesOf('Global', module)
    .add('buttons', () => (
        <div>
            <button className="btn btn-info">Info</button>
            <br />
            <button className="btn btn-danger">Danger</button>
        </div>
    ))
