import keyBy from 'lodash/keyBy';

export function getStepById(steps, id) {
    const stepById = keyBy(steps, 'id');
    return stepById[id] || null;
}

export function getStepFromEvent(event, stepId) {
    const stepById = keyBy(event.steps, 'id');
    return stepById[stepById] || null;
}

export function getEventById(events, id) {
    const eventById = keyBy(events, 'id');
    return eventById[id] || null;
}
