import slug from 'slug';

slug.defaults.mode = 'pretty';

export default function(str) {
    return slug(str).toLowerCase();
}
