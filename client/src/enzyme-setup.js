import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';

const dotenv = require('dotenv');
dotenv.config();

configure({ adapter: new Adapter() });
