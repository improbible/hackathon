import firebase from 'firebase';

import { firebaseConfig } from './config';

firebase.initializeApp(firebaseConfig);

function clearAll() {
    return firebase
        .database()
        .ref()
        .remove();
}

export { firebase, clearAll };
