import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { reactReduxFirebase } from 'react-redux-firebase';
import thunk from 'redux-thunk';
import debug from 'debug';

import rootSaga from './sagas';
import reducer from './store';

const log = debug('debug:store');

const logger = store => next => action => {
    log('dispatch', action);
    return next(action);
};

export default function(initialState, firebaseConfig) {
    let sagaMiddleware = createSagaMiddleware();

    const createStoreWithFirebase = compose(
        reactReduxFirebase(firebaseConfig, { userProfile: 'users' })
    )(createStore);

    const store = createStoreWithFirebase(
        reducer,
        initialState,
        applyMiddleware(logger, thunk, sagaMiddleware)
    );

    sagaMiddleware.run(rootSaga);

    store.subscribe(() => log('store changed', store.getState()));

    if (module.hot) {
        module.hot.accept('./store', () => {
            const reducer = require('./store').default;
            store.replaceReducer(reducer);
        });
    }

    return store;
}
