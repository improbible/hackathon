import React from 'react';
import { Route } from 'react-router';

import Hackathon from 'screens/hackathon';

export default [
    <Route path="/" component={Hackathon} />
];
