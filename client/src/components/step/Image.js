import React, { Component, PropTypes } from 'react';

function Image({ src }) {
    return (
        <div
            style={{
                width: '100vw',
                height: '100vh',
            }}
        >
            <img src={src} style={{ width: '100%' }} />
        </div>
    );
}

export default Image;
