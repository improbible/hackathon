import React, { Component, PropTypes } from 'react';
import Image from './Image';

const propTypes = {};

const defaultProps = {};

class BreakSubmitStep extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Image src="/public/assets/storyboard/6-2-entracte.png" />;
    }
}

BreakSubmitStep.propTypes = propTypes;
BreakSubmitStep.defaultProps = defaultProps;

export default BreakSubmitStep;
