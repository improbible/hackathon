import React, { Component, PropTypes } from 'react';
import Image from './Image';

const propTypes = {};

const defaultProps = {};

class BreakStep extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Image src="/public/assets/storyboard/6-1-entracte.png" />;
    }
}

BreakStep.propTypes = propTypes;
BreakStep.defaultProps = defaultProps;

export default BreakStep;
