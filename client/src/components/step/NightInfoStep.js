import React, { Component, PropTypes } from 'react';
import Image from './Image';

const propTypes = {};

const defaultProps = {};

class NightInfoStep extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Image src="/public/assets/storyboard/1-info-soiree.png" />;
    }
}

NightInfoStep.propTypes = propTypes;
NightInfoStep.defaultProps = defaultProps;

export default NightInfoStep;
