import React, { Component, PropTypes } from 'react';
import Image from './Image';

const propTypes = {};

const defaultProps = {};

class NextEventStep extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Image src="/public/assets/storyboard/0-prochain-event.png" />;
    }
}

NextEventStep.propTypes = propTypes;
NextEventStep.defaultProps = defaultProps;

export default NextEventStep;
