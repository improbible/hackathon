import React, { Component, PropTypes } from 'react';

const propTypes = {
    onSubmit: PropTypes.func.isRequired,
};

const defaultProps = {};

class ThemeGenerator extends Component {
    constructor(props) {
        super(props);
        this.state = { customTitle: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        const { onSubmit } = this.props;

        return (
            <form>
                <input
                    type="text"
                    name="customTitle"
                    onChange={this.handleChange}
                    value={this.state.customTitle}
                />
                <button onClick={this.handleSubmit}>Submit</button>
            </form>
        );
    }

    handleChange(evt) {
        this.setState({ customTitle: evt.target.value });
    }

    handleSubmit(evt) {
        evt.preventDefault();
        this.props.onSubmit({ customTitle: this.state.customTitle });
    }
}

ThemeGenerator.propTypes = propTypes;
ThemeGenerator.defaultProps = defaultProps;

export default ThemeGenerator;
