import React, { Component, PropTypes } from 'react';
import cn from 'classname';
import './vote.less';

const propTypes = {
    onSelect: PropTypes.func.isRequired,
};

const defaultProps = {};

class Vote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            team: null,
        };
    }
    vote(team) {
        this.setState({
            team,
        });
        this.props.onSelect(team);
    }
    render() {
        const { team } = this.state;
        const hasTeamSelected = team !== null;

        return (
            <div className="vote-component container">
                <div
                    className={cn({
                        hide: !hasTeamSelected,
                        back: hasTeamSelected,
                    })}
                    onClick={() => {
                        this.setState({ team: null });
                    }}
                >
                    <i className="fa fa-arrow-left  fa-3x" />
                </div>
                <div
                    className={cn({
                        left: true,
                        hide: team === 'blue',
                        fullsize: team === 'red',
                    })}
                    onClick={() => {
                        this.vote('red');
                    }}
                >
                    <p>Tremblay</p>
                </div>
                <div
                    className={cn({
                        right: true,
                        hide: team === 'red',
                        fullsize: team === 'blue',
                    })}
                    onClick={() => {
                        this.vote('blue');
                    }}
                >
                    <p>Bouchard</p>
                </div>
            </div>
        );
    }
}

Vote.propTypes = propTypes;
Vote.defaultProps = defaultProps;

export default Vote;
