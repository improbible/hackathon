import React, { Component, PropTypes } from 'react';

const propTypes = {
    onSubmit: PropTypes.func.isRequired,
};

const defaultProps = {};

class ImproForm extends Component {
    constructor(props) {
        super(props);
        this.state = { title: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        const { onSubmit } = this.props;

        return (
            <form>
                <input
                    type="text"
                    name="title"
                    onChange={this.handleChange}
                    value={this.state.title}
                />
                <button onClick={this.handleSubmit}>Submit</button>
            </form>
        );
    }

    handleChange(evt) {
        this.setState({ title: evt.target.value });
    }

    handleSubmit(evt) {
        evt.preventDefault();
        this.props.onSubmit({ title: this.state.title });
    }
}

ImproForm.propTypes = propTypes;
ImproForm.defaultProps = defaultProps;

export default ImproForm;
