import React from 'react';
import PieChart from 'react-minimal-pie-chart';
import CountdownNow from 'react-countdown-now';

require('./Countdown.less')

function Timer({ minutes }) {
    return <h1>{formatTime(minutes)}</h1>;
}
function formatTime(duration) {
    var hour = Math.floor(duration / 60);
    var min = duration - hour * 60;

    return (
        (hour < 10 ? '0' + hour.toString() : hour.toString()) +
        ':' +
        (min < 10 ? '0' + min.toString() : min.toString())
    );
}

function completedPercentage(deadline, now, startAt) {
    if(now <= startAt) {
        return 100;
    }
    else if(now >= deadline) {
        return 0;
    }

    const diff = now - startAt;
    const length = deadline - startAt;

    return Math.floor(diff / length * 100);
}

function Countdown({deadline, startAt}) {
    function renderer({ hours, minutes, seconds, completed }) {
        const percentage = completedPercentage(deadline.getTime(), Date.now(), startAt.getTime());
        let totalMinutes = parseInt(hours, 10) * 60 + parseInt(minutes, 10);

        if(parseInt(seconds, 10) > 0) {
            totalMinutes += 1;
        }

        if(completed) {
            return <div className="text-center" style={{marginTop: '3rem'}}>
                    <h1 style={{fontSize: '5rem'}}>🎉</h1>
                    <div style={{marginTop: '1rem', fontSize: '2rem', fontWeight: 'bold'}}>Terminé!</div>
                </div>
        }

        return <PieChart
            className="bigCircle"
            data={[{ value: 1, color: '#13dcb4' }]}
            reveal={100 - percentage}
            startAngle={270}
        >
            <div className="smallCircle">
                <Timer minutes={totalMinutes} />
            </div>
        </PieChart>
    }

    return (
        <div className="countdown">
            <CountdownNow
                date={deadline}
                now={() => {
                    const now = Date.now();
                    if(now < startAt.getTime()) {
                        return startAt.getTime();
                    }

                    return now;
                }}
                renderer={renderer}
                daysInHours
            />
        </div>
    )
}

export default Countdown;