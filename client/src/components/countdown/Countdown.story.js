import React from 'react';
import { storiesOf } from '@storybook/react';
import Countdown from './Countdown';

storiesOf('Countdown', module)
    .add('simple countdown', () => (
        <Countdown
            startAt={new Date("2018-09-26T20:00:00.000Z")}
            deadline={new Date("2018-09-28T18:00:00.000Z")}
        />
    ))
