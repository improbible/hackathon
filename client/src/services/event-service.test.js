import EventService from './event-service';
import OrganizationService from './organization-service';

import { firebase, clearAll } from '../firebase-mock-setup';

const eventService = EventService.connect(firebase);
const organizationService = OrganizationService.connect(firebase);

afterEach(() => {
    return clearAll();
});

it('should create an event', done => {
    return organizationService
        .create({ name: 'my org' })
        .then(org => {
            return eventService.create({
                organizationId: org.id,
                name: 'awesome event',
                date: new Date().toISOString(),
                locationName: 'Bell center'
            });
        })
        .then(event => {
            expect(event.id).toBeDefined();
            expect(event.organizationId).toBeDefined();
            expect(event.name).toBe('awesome event');
            expect(event.locationName).toBe('Bell center');

            done();
        })
        .catch(err => {
            console.log(err);
        });
});

it('should not create an event with an invalid organization id', done => {
    eventService
        .create({
            organizationId: 'invalid-id',
            name: 'awesome event',
            date: new Date().toISOString(),
            locationName: 'Bell center'
        })
        .catch(err => {
            expect(err).toBe('organization_not_found');
            done();
        });
});
