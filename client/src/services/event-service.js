import uuid from './../utils/uuid';
import { EVENTS, ORGANIZATIONS } from './collections';

function eventService(firebase) {
    function create({ name, organizationId, date, locationName }) {
        const organizationRef = firebase
            .database()
            .ref(`${ORGANIZATIONS}/${organizationId}`);

        return organizationRef
            .once('value')
            .then(snapshot => {
                if (!snapshot.val()) {
                    throw 'organization_not_found';
                }
            })
            .then(() => {
                const eventId = uuid();
                const event = {
                    id: eventId,
                    name,
                    organizationId,
                    date,
                    activeStep: null,
                    steps: [],
                    locationName,
                    teamRed: null,
                    teamBlue: null
                };

                const updates = {
                    [`${ORGANIZATIONS}/${organizationId}/events/${eventId}`]: true,
                    [`${EVENTS}/${eventId}`]: event
                };

                return firebase
                    .database()
                    .ref()
                    .update(updates)
                    .then(() => {
                        return event;
                    });
            });
    }

    function gotoStep({ eventId, stepId }) {
        return firebase
            .database()
            .ref(`${EVENTS}/${eventId}`)
            .update({
                activeStep: stepId
            });
    }

    function registerVote({ eventId, team }) {
        return firebase
            .database()
            .ref(`${EVENTS}/${eventId}`)
            .transaction(event => {
                if (event) {
                    if (!event.vote) {
                        event.vote = {
                            red: 0,
                            blue: 0
                        };
                    }

                    event.vote[team]++;
                }

                return event;
            });
    }
    return {
        create,
        gotoStep,
        registerVote
    };
}

function connect(firebase) {
    if (!firebase) {
        throw 'firebase is not defined!';
    }

    return eventService(firebase);
}

export default {
    connect
}
