function improService(firebase) {
    function startTimer(stepId) {
        return firebase
            .database()
            .ref(`steps/${stepId}`)
            .update({
                status: 'running',
                timerStartedAt: new Date().toISOString(),
            });
    }

    function endTimer(stepId) {
        return firebase
            .database()
            .ref(`steps/${stepId}`)
            .update({
                status: 'pending',
                timerStartedAt: null,
            });
    }

    function startVote(stepId) {
        return firebase
            .database()
            .ref(`steps/${stepId}`)
            .update({
                status: 'voting',
                voteStartedAt: new Date().toISOString(),
            });
    }

    function endVote(stepId) {
        return firebase
            .database()
            .ref(`steps/${stepId}`)
            .update({
                status: 'pending',
                voteStartedAt: null,
            });
    }

    function voteForTeam(stepId, teampId) {}

    return {
        startTimer,
        endTimer,
        startVote,
        endVote,
        voteForTeam,
    };
}

function connect(firebase) {
    if (!firebase) {
        throw 'firebase is not defined!';
    }

    return improService(firebase);
}

module.exports = {
    connect,
};
