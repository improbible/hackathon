let themes = null;

export const selectTheme = callback => {
    if (!themes) {
        fetch('/public/themes.json').then(response => {
            themes = response.json();
            getRandomTheme(me.themes, callback);
        });
    } else {
        getRandomTheme(me.theme, callback);
    }
};

function getRandomTheme(themes, callback) {
    var index = Math.floor(Math.random() * themes.length);
    callback(themes[index]);
}
