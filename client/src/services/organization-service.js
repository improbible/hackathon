import slugify from './../utils/slugify';

import { ORGANIZATIONS, ORGANIZATION_SLUGS } from './collections';

function organizationService(firebase) {
    function create({ name }) {
        const slug = slugify(name);

        const organizationSlugRef = firebase
            .database()
            .ref(`${ORGANIZATION_SLUGS}/${slug}`);

        return organizationSlugRef
            .once('value')
            .then(snapshot => {
                if (snapshot.val()) {
                    throw 'slug_exists';
                }

                return null;
            })
            .then(() => {
                const newOrganizationRef = firebase
                    .database()
                    .ref()
                    .child(ORGANIZATIONS)
                    .push();

                const id = newOrganizationRef.key;
                const organization = {
                    id,
                    slug,
                    name,
                    events: {}
                };

                const updates = {
                    [`${ORGANIZATION_SLUGS}/${slug}`]: id,
                    [`${ORGANIZATIONS}/${id}`]: organization
                };

                return firebase
                    .database()
                    .ref()
                    .update(updates)
                    .then(() => {
                        return organization;
                    });
            });
    }

    return {
        create
    };
}

function connect(firebase) {
    if (!firebase) {
        throw 'firebase is not defined!';
    }

    return organizationService(firebase);
}

module.exports = {
    connect
};
