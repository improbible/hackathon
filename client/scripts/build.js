const fs = require('fs-extra');
const path = require('path');
const webpack = require('webpack');

const config = require('./../webpack.config');

const compiler = webpack(config);

const compile = statsFormat => {
    console.log('Start webpack compile.');
    return new Promise((resolve, reject) => {
        compiler.run((err, stats) => {
            if (err) {
                console.log('Webpack compiler encountered a fatal error.', err);
                return reject(err);
            }

            const jsonStats = stats.toJson();
            console.log('Webpack compile completed.');
            console.log(stats.toString(statsFormat));

            if (jsonStats.errors.length > 0) {
                console.log('Webpack compiler encountered errors.');
                console.log(jsonStats.errors.join('\n'));
                return reject(new Error('Webpack compiler encountered errors'));
            } else if (jsonStats.warnings.length > 0) {
                console.log('Webpack compiler encountered warnings.');
                console.log(jsonStats.warnings.join('\n'));
            } else {
                console.log('No errors or warnings encountered.');
            }

            resolve(jsonStats);
        });
    });
};

compile({
    chunks: true,
    chunkModules: true,
    colors: true,
})
    .then(stats => {
        // Fail on warnings
        if (stats.warnings.length) {
            throw new Error(
                'Config set to fail on warning, exiting with status code "1".'
            );
        }
    })
    .then(() => {
        console.log('Copying static assets to dist folder.');
        fs.copySync(
            path.join(__dirname, '..', 'public'),
            path.join(__dirname, '..', '.dist', 'public')
        );
        fs.copySync(
            path.join(__dirname, '..', 'index.html'),
            path.join(__dirname, '..', '.dist', 'index.html')
        );
    })
    .then(() => {
        console.log('Compilation completed successfully.');
    })
    .catch(err => {
        console.log('Compiler failed.', err);
        process.exit(1);
    });
