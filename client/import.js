var firebase = require('firebase');
var config = require('./../firebase.json');

firebase.initializeApp(config);
var database = firebase.database();

if(database ) {
    console.log("datbase")
}

const STEP = '/steps';
const EVENT = '/events';
const ORGANIZATION = '/organizations';

var stepRefs = database.ref(STEP)
var eventRefs = database.ref(EVENT)
var organizationRefs = database.ref(ORGANIZATION)

const keys = []
const updates = {};
const steps = [
    'rating',
    'break1',
    'nightinfo',
    'break2',
    'nextevent'
].map((type) => {
    return {type: type}
})

steps.map((step) => {
    const key = stepRefs.push().key
    keys.push(key)
    step.id = key;
    console.log(key, step.type);

    updates[`${STEP}/${key}`] = step
})

database.ref().update(updates)
